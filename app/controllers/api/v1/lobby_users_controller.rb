module Api
  module V1
    class LobbyUsersController < ApiController
      
      before_action :find_resources, only: %w(index)
      before_action :new_resource, only: %w(create new)
      before_action :find_resource, only: %w(show update destroy edit)

      def create
        result = ::JoinLobbyInteractor.call(lobby_user_params)
        @response = {errors: []}

        if @response[:success] = result.success?
          @response[:lobby_id] = result.lobby_user.lobby_id
        else
          @response[:errors] << result.error
        end
    
        render json: @response
      end

      def lobby_user_params
        {
          user: current_user, lobby_id: params[:lobby_id]
        }
      end
      
      def resource_class
        LobbyUser
      end

    end
  end
end
