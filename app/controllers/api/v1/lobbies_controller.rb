module Api
  module V1
    class LobbiesController < ApiController
      
      before_action :find_resources, only: %w(index)
      before_action :new_resource, only: %w(create new)
      before_action :find_resource, only: %w(show update destroy edit leave)
      
      def leave
        result = ::LeaveLobbyInteractor.call(leave_lobby_params)

        @response = {errors: []}

        if not @response[:success] = result.success?
          @response[:errors] << result.error
        end
        
        render json: @response
      end

      def start
        result = ::StartLobbyInteractor.call(start_lobby_params)
        @response = {errors: []}

        if not @response[:success] = result.success?
          @response[:errors] << result.error
        end
    
        render json: @response
      end


      def leave_lobby_params
        {lobby: Lobby.find(params[:id]), user: current_user}
      end
      def start_lobby_params
        { id: params[:id] }
      end

      def resource_class
        Lobby
      end

    end
  end
end
