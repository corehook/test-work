class ApplicationController < ActionController::Base
  before_action :authenticate_user!

  def serialize_resources(resources, serializer, scope = {})
    ActiveModelSerializers::SerializableResource.new(
        resources,
        each_serializer: serializer,
        root: false,
        scope: scope
    ).serializable_hash
  end

  def serialize_resource(resource, serializer, scope = {})
    ActiveModelSerializers::SerializableResource.new(
        resource,
        serializer: serializer,
        root: false,
        scope: scope
    ).serializable_hash
  end
end
