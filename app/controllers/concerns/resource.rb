module Concerns::Resource
  extend ActiveSupport::Concern

  included do
    # before_action :dump_resource_params, only: [:create, :update]
  end

  def dump_resource_params
    logger.info "Resource #{resource_symbol} params : #{resource_params.to_h}"
  end

  def resource_serializer
    resource_class.serializer
  end

  def resource_scope
    resource_class.all
  end

  def permitted_params
    resource_class.permitted_params
  end

  def find_by_ids
    @resources = resource_scope.find(params[:ids])
  end

  def added_params
    {}
  end

  def resource_attribute_kind
    :id
  end

  def resource_attribute_params
    params[:id]
  end

  # Methods
  def new_resource
    @resource ||= resource_scope.new resource_params

    if resource_scope.attribute_names.include? 'user_id'
      @resource.user = current_user
    end

  end

  def current_user_scope
    if resource_class.attribute_names.include? 'user_id' and current_user.operator?
      resource_class.where(user: current_user)
    else
      resource_class.all
    end
  end

  def resource_symbol
    resource_scope.name.underscore.to_sym
  end

  def find_resource
    @resource ||= resource_scope.find_by(resource_attribute_kind => resource_attribute_params)
  end

  def find_resources
    @resources ||= search_proxy
  end

  def search_by
    [ :id ]
  end

  def search_proxy
    resource_scope
  end

  def resource_params
    if params[resource_symbol] && permitted_params
      params.require(resource_symbol).permit(permitted_params).merge(added_params)
    elsif params[resource_symbol] && permitted_params.nil?
      params.require(resource_symbol).permit!.merge(added_params)
    else
      added_params
    end
  end

  # Actions
  def send_json(obj, success)
    render json: obj, status: success ? 200 : 403
  end

  def index
    send_json serialize_resources(@resources, resource_serializer ), true
  end

  # def create
  #   logger.info "\n"*3
  #   logger.info "Create resource #{resource_symbol} with #{resource_params.to_json}"
  #   send_json @resource, @resource.save
  # end

  def create
    logger.info "Create resource #{resource_symbol} with #{resource_params.to_json}"
    result = @resource.save
    send_json serialize_resource(@resource, resource_serializer), result
  end

  def update
    result = nil
    logger.info "Update resource #{resource_symbol}##{@resource.id} with #{resource_params.to_json}"
    if @resource
      result = @resource.update_attributes(resource_params)
    end

    if not result
      logger.info "Errors while update resource #{@resource.errors.full_messages}"
    end

    send_json serialize_resource(@resource, resource_serializer),result
  end

  def new
    send_json @resource, true
  end

  def show
    send_json serialize_resource(@resource, resource_serializer ), true
  end

  def destroy
    send_json @resource, @resource.destroy
  end
end
