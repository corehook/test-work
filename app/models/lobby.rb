class Lobby < ApplicationRecord
  include Resourcable
  after_create :create_lobby_user_for_owner
  after_create :broadcast_lobbies
  after_update :broadcast_lobbies
  
  validates :players_count, inclusion: { in: [2,4] }
  validates :name, presence: true
  belongs_to :user
  has_many :lobby_users
  enum status: [ :performed, :started, :finished ]
  
  private

  def self.permitted_params
    [ :players_count, :settings, :name ]
  end

  def create_lobby_user_for_owner
    LobbyUser.create(lobby: self, user: user)
  end


end
