class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  has_many :lobby_users
  
  def online!
    self.update(online: true)
  end

  def offline!
    self.update(online: false)
  end
end
