module Resourcable
  extend ActiveSupport::Concern

  included do

    def self.default_scope
      self.all
    end

    def self.serializer
      [self.name,"Serializer"].join.constantize
    end

    def permitted_params
      [ ]
    end

    def serialize_resources(resources, serializer, scope = {})
      ActiveModelSerializers::SerializableResource.new(
          resources,
          each_serializer: serializer,
          root: false,
          scope: scope
      ).serializable_hash
    end

    def serialize_resource(resource, serializer, scope = {})
      ActiveModelSerializers::SerializableResource.new(
          resource,
          serializer: serializer,
          root: false,
          scope: scope
      ).serializable_hash
    end

  end


end
