class LobbyUser < ApplicationRecord
  include Resourcable

  after_create :broadcast_lobbies
  after_update :broadcast_lobbies

  belongs_to :user
  belongs_to :lobby
  
  validates_uniqueness_of :user_id, scope: :lobby_id
  enum status: [ :in_game, :leaved ]
  
  def self.permitted_params
    [ :lobby_id, :user_id]
  end

end
