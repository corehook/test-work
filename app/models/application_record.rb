class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  def serialized
    ActiveModelSerializers::SerializableResource.new(
        self,
        serializer: [self.class.name,"Serializer"].join.constantize,
        root: false
    ).serializable_hash
  end

  def broadcast_lobbies
    ActionCable.server.broadcast 'LobbyChannel', {
      data: serialize_resources(Lobby.all, Lobby.serializer),
      data_type: 'lobbies'
    }
  end
end
