class LobbyGamesWorker
  include Sidekiq::Worker

  def perform
    Lobby.started.each do |started_lobby|
      if started_lobby.started_at > 1.minute.ago
        if started_lobby.update(status: :finished, finished_at: DateTime.now)
          ActionCable.server.broadcast "LobbyChannel", {
            data: started_lobby.serialized,
            data_type: 'lobby_finished'
          }
        end
        
      end
    end
  end
end