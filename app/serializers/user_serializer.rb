class UserSerializer < BaseSerializer
  attributes :id, :email, :online
end