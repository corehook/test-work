class LobbySerializer < BaseSerializer
  attributes :settings, :status, :id, :user, :players_count, :in_game_players_count, :leaved_players_count,
    :joined, :leaved, :owner

  has_many :lobby_users, serialier: LobbyUserSerializer
  
  def in_game_players_count
    object.lobby_users.in_game.count
  end

  def leaved_players_count
    object.lobby_users.leaved.count
  end

  def joined
    false
  end
  def leaved
    false
  end
  def owner
    false
  end
end
