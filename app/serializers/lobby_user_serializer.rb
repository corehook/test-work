class LobbyUserSerializer < BaseSerializer
  attributes :id, :user, :lobby, :status
end
