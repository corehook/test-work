class BaseSerializer < ActiveModel::Serializer
  attributes :id, :valid

  def valid
    object.errors.full_messages.length <= 0
  end
end
