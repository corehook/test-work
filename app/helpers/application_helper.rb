module ApplicationHelper
  def user_meta_tag
    tag.meta name: 'user-context', id: current_user.id
  end
end
