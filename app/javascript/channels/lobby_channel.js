import consumer from "./consumer"
import store from '../src/store';

consumer.subscriptions.create("LobbyChannel", {
  connected() {
    console.log('connected');
    // Called when the subscription is ready for use on the server
  },

  disconnected() {
    console.log('disconnected');
    // Called when the subscription has been terminated by the server
  },

  received(data) {
    console.log('received');
    // Called when there's incoming data on the websocket for this channel
  }
});
