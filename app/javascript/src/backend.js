import axios from 'axios';
const token = document.querySelector('[name="csrf-token"]') || {content: 'no-csrf-token'}

export const HTTP = axios.create({
  baseURL: `http://localhost:3000/api/v1/`,
  headers: {
    common: {
      'X-CSRF-Token': token.content
    }
  }
})