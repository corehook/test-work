import Vue from 'vue'
import VueRouter from 'vue-router';

Vue.use(VueRouter);

import NewLobby from './components/lobby/new.vue';
import LobbiesIndex from './components/lobby/index.vue';
import LobbyShow from './components/lobby/show.vue';

const router = new VueRouter({
  mode: 'history',
  routes: [
    { path: '/',          component: LobbiesIndex,  name: 'root_path'       },
    { path: '/new',       component: NewLobby,      name: 'new_lobby_path'  },
    { path: '/lobby/:id', component: LobbyShow,     name: 'lobby_show'      },
  ]
});

export default router;