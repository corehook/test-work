import {HTTP} from '../../backend'

// initial state
const state = {
  lobbies: [],
  user: {},
  users: [],
  user_id: null
}

// getters
const getters = {
  get_user_id: function(state) {
    return state.user_id;
  },
  get_users: function(state) {
    return state.users;
  },
  get_lobbies: function(state) {
    return state.lobbies;
  },
}

// actions
const actions = {
  set_user_id(state, user_id) {
    state.commit('set_user_id', user_id);
  },
  ws_received(state, payload) {
    // console.log('ws_recieved');
    // console.log(payload);

    let data = payload.data;
    let data_type = payload.data_type;

    switch( data_type ) { 

      case "lobbies": { 

        // dirty check for joined user
        // TODO: refactor this shit man wtf :/
        data.forEach(function (lobby, index) {

          lobby.lobby_users.forEach(function(lobby_user, i){
            if(lobby.user.id == state.state.user_id){
              lobby.owner = true;
            }

            if(lobby_user.user.id == state.state.user_id){
              if(lobby_user.status == 'in_game'){
                lobby.joined = true;
              } else if(lobby_user.status == 'leaved'){
                lobby.leaved = true;
              }
              // console.log(lobby_user);
              
            }            
          });
          
        });
        
        state.commit('set_lobbies', data);
        break; 
      }  
      
      case "users": {
        state.commit('set_users', data);
        break;
      }
      
      case "lobby": {
        state.commit('add_lobby', data);
        break;
      }
      
      case "lobby_user": {
        console.log("lobby_user"); 
        console.log(data);
        break;
        // state.state.lobbies.forEach(function(lobby, index) {
        //   if(lobby.id == data.lobby.id){
        //     lobby.lobby_users = lobby_users.map(x => (x.id === data.id) ? data : x)
        //   }
        // });
        // items = items.map(x => (x.id === item.id) ? item : x)
      }

      default: { 
          console.log("Invalid data_type"); 
          console.log(data)
          break;              
      } 
    } 

  },
  fetch_lobbies(state) {
    HTTP.get(`lobbies/`).then(response => {
      state.commit('set_lobbies', response.data);
    })
  }
}

// mutations
const mutations = {
  add_lobby: function(state, lobby) {
    state.lobbies.push(lobby);
  },
  set_lobbies: function(state, payload) {
    state.lobbies = payload;
  },
  set_users: function(state, payload) {
    state.users = payload;
  },
  set_user_id: function(state, payload) {
    state.user_id = payload;
  }
}

export default {
  namespaced: false,
  state,
  getters,
  actions,
  mutations
}