import Vue from 'vue'
import { MLInstaller, MLCreate, MLanguage } from 'vue-multilanguage';

Vue.use(MLInstaller)


export default new MLCreate({
    initial: 'english',
    save: process.env.NODE_ENV === 'production',
    languages: [
      new MLanguage('english').create({
        online_users: 'Online {0}',
        lobby_for_4: 'For 4 players',
        lobby_for_2: 'For 2 players',
        owner: 'Owner',
        settings: 'Settings',
        status: 'Status',
        email: 'Email',
        create_new_lobby: 'Create lobby',
        lobby_name: 'Lobby name',
        lobby_players_count: 'Lobby players count',
        create: 'Create',
        errors_invalid_lobby_name: 'Please fill lobby name',
        errors_invalid_lobby_players_count: 'Please select lobby players number',
        lobby_list: 'Lobby list',
        lobby_show: 'Show lobby',
        performed: 'Waiting for players',
        started: 'Started',
        finished: 'Finished',
        players: 'players',
        started_at: 'started at',
        finished_at: 'finished at',
        created_at: 'created at: ',
        join_lobby: 'Join lobby',
        leave_lobby: 'Leave lobby',
        you_leaved_lobby: 'Leaved',
        start_game: 'Start game',
        ready_to_start: 'Ready to start!'
      }),
   
      new MLanguage('russian').create({
        online_users: 'Онлайн {0}',
        lobby_for_4: 'Для 4 игроков',
        lobby_for_2: 'Для 2 игроков',
        owner: 'Владелец',
        settings: 'Настройки',
        status: 'Статус',
        email: 'Почта',
        create_new_lobby: 'Создать лобби',
        lobby_name: 'Название лобби',
        lobby_players_count: 'Количество игроков',
        create: 'Создать',
        errors_invalid_lobby_name: 'Укажите название лобби',
        errors_invalid_lobby_players_count: 'Выберите количество игроков',
        lobby_list: 'Список лобби',
        lobby_show: 'Просмотреть',
        performed: 'Набор участников',
        started: 'Игра запущена',
        finished: 'Игра закончена',
        players: 'игроков',
        started_at: 'Время запуска',
        finished_at: 'Закончилась в ',
        created_at: 'Время создания',
        join_lobby: 'Войти в лобби',
        leave_lobby: 'Покинуть лобби',
        you_leaved_lobby: 'Вы покинули лобби',
        start_game: 'Запустить игру',
        ready_to_start: 'Готова к запуску'

      })
    ]
  })