/* eslint no-console: 0 */
// Run this example by adding <%= javascript_pack_tag 'hello_vue' %> (and
// <%= stylesheet_pack_tag 'hello_vue' %> if you have styles in your component)
// to the head of your layout file,
// like app/views/layouts/application.html.erb.
// All it does is render <div>Hello Vue</div> at the bottom of the page.

import Vue from 'vue';
import Vuex from 'vuex';
import ActionCableVue from 'actioncable-vue';
import App from '../src/components/layout.vue';
import router from '../src/router';
import '../src/ml';
import store from '../src/store';

Vue.use(Vuex);
Vue.use(ActionCableVue, {
  debug: true,
  debugLevel: 'error',
  connectionUrl: 'ws://localhost:3000/cable',
  connectImmediately: true,
});

document.addEventListener('DOMContentLoaded', () => {
  
  const app = new Vue({
    router,  
    store,
    render: h => h(App)
  }).$mount()
  document.body.appendChild(app.$el)

  console.log(app)
})

