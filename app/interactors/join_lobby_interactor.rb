class JoinLobbyInteractor
  include Interactor
  
  #
  # Params
  # @user
  # @lobby
  def call
    check_user_join_limits
    check_lobby
    create_lobby_user
  end

  def create_lobby_user
    context.lobby_user = LobbyUser.create(user: user, lobby_id: context.lobby_id)
  end

  def check_lobby
    context.fail!(error: 'Game started') if lobby.started?
    context.fail!(error: 'Lobby is full') if lobby.lobby_users.in_game.count >= lobby.players_count
  end

  def check_user_join_limits
    if user.lobby_users.leaved.where('leaved_at > ?', 1.minute.ago).count > 0
      context.fail!(error: 'Banned for 1 minute')
    end
  end

  def lobby
    Lobby.find(context.lobby_id)
  end

  def user
    context.user
  end
end