class LeaveLobbyInteractor
  include Interactor
  
  def call
    if lobby_user.update(status: :leaved, leaved_at: DateTime.now)

      ActionCable.server.broadcast "LobbyChannel", {
        data: lobby.serialized,
        data_type: 'lobby',
        action_type: 'lobby_leaved'
      }
    else
      context.fail!(error: 'Cant leave lobby') 
    end
  end

  def lobby_user
    lobby.lobby_users.where(user: context.user).first
  end

  def lobby
    context.lobby
  end
end