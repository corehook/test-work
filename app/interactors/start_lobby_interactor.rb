class StartLobbyInteractor
  include Interactor
  
  def call
    context.fail!(error: 'not enough players') if lobby.players_count != lobby.lobby_users.in_game.count
    context.fail!(error: 'lobby already started') if lobby.started?
    context.fail!(error: 'cant start lobby') if not lobby.update(status: :started, started_at: DateTime.now)

    ActionCable.server.broadcast "LobbyChannel", {
      data: lobby.serialized,
      data_type: 'lobby_started'
    }
  end

  def lobby
    context.lobby = Lobby.find(context.id)
  end

end