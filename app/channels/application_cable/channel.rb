module ApplicationCable
  class Channel < ActionCable::Channel::Base


    def serialize_resources(resources, serializer)
      ActiveModelSerializers::SerializableResource.new(
          resources,
          each_serializer: serializer,
          root: false
      ).serializable_hash
    end

    def serialize_resource(resource, serializer)
      ActiveModelSerializers::SerializableResource.new(
          resource,
          serializer: serializer,
          root: false
      ).serializable_hash
    end

    
  end
end
