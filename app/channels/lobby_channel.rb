class LobbyChannel < ApplicationCable::Channel
  def subscribed
    stream_from "LobbyChannel"

    current_user.online!
    broadcast_lobbies
    broadcast_online_users
  end

  def index
    broadcast_lobbies
    broadcast_online_users
  end

  def unsubscribed
    current_user.offline!
    broadcast_online_users
  end

  def broadcast_online_users
    ActionCable.server.broadcast 'LobbyChannel', {
      data: serialize_resources(User.where(online: true), UserSerializer),
      data_type: 'users'
    }
  end
  def broadcast_lobbies
    ActionCable.server.broadcast 'LobbyChannel', {
      data: serialize_resources(Lobby.all, LobbySerializer),
      data_type: 'lobbies'
    }
  end

end
