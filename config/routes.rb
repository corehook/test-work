Rails.application.routes.draw do
  root to: 'lobby#index'
  devise_for :users
  
  
  namespace :api do
    namespace :v1 do
      resources :lobbies do 
        post :leave
        post :start
      end
      resources :lobby_users
    end
  end

end
