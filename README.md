# README

# requirements
node - v8.17.0
yarn - 1.22.0
ruby - 2.6.0p0
redis
postgresql

# Prepare project dev
```bash
$ git clone git@gitlab.com:corehook/test-work.git
$ bundle install 
$ yarn install --check-files
$ rake db:create db:migrate
```

# Start
```
$ rake db:create db:migrate
$ docker-compose build
$ docker-compose up
$ foreman start
```