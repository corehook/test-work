# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# 
# User.create(email: 'corehook4@gmail.com', password: 'corehook')
# User.create(email: 'corehook3@gmail.com', password: 'corehook')
# User.create(email: 'corehook2@gmail.com', password: 'corehook')
LobbyUser.destroy_all
Lobby.destroy_all
Lobby.create(user: User.first, players_count: 4, name: 'Test')
User.all.map { |u| LobbyUser.create(user: u, lobby: Lobby.first) }
