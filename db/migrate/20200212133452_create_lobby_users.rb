class CreateLobbyUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :lobby_users do |t|
      t.references :user, null: false, foreign_key: true
      t.references :lobby, null: false, foreign_key: true

      t.timestamps
    end
  end
end
