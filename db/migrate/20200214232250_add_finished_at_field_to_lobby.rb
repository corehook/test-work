class AddFinishedAtFieldToLobby < ActiveRecord::Migration[6.0]
  def change
    add_column :lobbies, :finished_at, :datetime
  end
end
