class AddPlayersCountFieldToLobby < ActiveRecord::Migration[6.0]
  def change
    add_column :lobbies, :players_count, :integer
  end
end
