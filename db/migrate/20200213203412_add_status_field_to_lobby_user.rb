class AddStatusFieldToLobbyUser < ActiveRecord::Migration[6.0]
  def change
    add_column :lobby_users, :status, :integer, default: 0
    add_column :lobby_users, :leaved_at, :datetime, nil: true
  end
end
