class AddStartedAtFieldToLobby < ActiveRecord::Migration[6.0]
  def change
    add_column :lobbies, :started_at, :datetime
  end
end
