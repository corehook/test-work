class AddNameFieldToLobby < ActiveRecord::Migration[6.0]
  def change
    add_column :lobbies, :name, :string
  end
end
