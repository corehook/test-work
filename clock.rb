require 'clockwork'
require 'active_support/time' # Allow numeric durations (eg: 1.minutes)
require_relative './config/boot'
require_relative './config/environment'
require_relative './app/workers/lobby_games_worker.rb'

module Clockwork
  include Sidekiq::Worker
  every(1.minutes, 'lobby_games_worker') { ::LobbyGamesWorker.perform_async }
end