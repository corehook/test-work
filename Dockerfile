FROM ruby:2.6.0

ENV RAILS_LOG_TO_STDOUT true

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs \
 && rm -rf /var/lib/apt/lists/* \
 && curl -o- -L https://yarnpkg.com/install.sh | bash

RUN mkdir /test_game
WORKDIR /test_game
COPY Gemfile /test_game/Gemfile
COPY Gemfile.lock /test_game/Gemfile.lock
RUN bundle install
COPY . /test_game
